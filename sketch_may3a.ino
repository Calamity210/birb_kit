
#include <LiquidCrystal.h>
#include <dht.h>

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

dht DHT;

#define DHT11_PIN 7

int light = 0;

void setup() {
  lcd.begin(16, 2);
  pinMode(9, OUTPUT); // configure digital pin 13 as an output
  pinMode(8, OUTPUT);
}

void loop() {
  int chk = DHT.read11(DHT11_PIN);
  lcd.setCursor(0,0); 
  lcd.print("Temp: ");
  lcd.print((int)DHT.temperature);
  lcd.print((char)223);
  lcd.print("C");
  lcd.setCursor(0,1);
  lcd.print("Humidity: ");
  lcd.print((int)DHT.humidity);
  lcd.print("%");

    light = analogRead(A0); 
 
    if(light > 450) { 
        digitalWrite(9,LOW); //turn left LED off
        digitalWrite(8,LOW); // turn right LED off
    }
    else {
        digitalWrite(9,HIGH); // Turn left LED on
        digitalWrite(8,HIGH); // Turn right LED on
    }
  delay(1000);
}
